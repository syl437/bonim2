'use strict';
angular.module('main')
    .controller('ApartmentCreateCtrl', function ($timeout, $ionicScrollDelegate, $ionicPopup, $localStorage, WizardHandler, $ionicModal, $log, $q, $scope, $state, $stateParams, FileService, Restangular, Utils, apartments, building, plan_categories, product_categories) {

        // Data

        $scope.apartments = apartments;
        $scope.building = building;
        $scope.choice = 0;
        $scope.selection = 1;
        $scope.emptyApartments = findEmptyApartments();
        $scope.product_categories = product_categories;
        $scope.plan_categories = plan_categories;
        $scope.selected = {
            apartment: null,   // 2 step = apartment data
            apartments: [], // 1 step = apartments numbers
            products: []   // 3 step = products numbers
        };
        $scope.apartmentData = Restangular.restangularizeElement(Restangular.one('buildings', building.id), {}, 'apartments');   // 2 step - info about apartment
        $scope.product_category = null;
        $scope.newProducts = Restangular.restangularizeCollection('', [], 'products');
        $scope.newPlans = {};
        $scope.productsToSave = [];
        $scope.savedApartments = [];
        $scope.savedPlans = [];

        // Methods

        $scope.onSave = onSave;
        $scope.makeFinish = makeFinish;
        $scope.showProductsModal = showProductsModal;
        $scope.closeProductsModal = closeProductsModal;
        $scope.getStep = function getStep (){return WizardHandler.wizard().currentStepNumber();};
        $scope.stepBack = function stepBack(){return WizardHandler.wizard().previous()};
        $scope.changeChoice = function changeChoice(x){$scope.choice = x;};
        $scope.changeSelection = changeSelection;
        $scope.selectApartment = selectApartment;
        $scope.deselectApartment = deselectApartment;
        $scope.isApartmentSelected = isApartmentSelected;
        $scope.selectProduct = selectProduct;
        $scope.deselectProduct = deselectProduct;
        $scope.isProductSelected = isProductSelected;
        $scope.isStep1Valid = isStep1Valid;
        $scope.isStep2Valid = isStep2Valid;
        $scope.isStep3Valid = isStep3Valid;
        $scope.getTitle = getTitle;
        $scope.uploadFile = uploadFile;

        // Functions

        function changeSelection(x){

            $scope.selection = x;
            $ionicScrollDelegate.scrollTop();

        }

        function isStep1Valid() {

           if ($scope.selected.apartments.length > 0)
               return true;

           return false;

        }

        function isStep2Valid(index) {

            if (index == 0){

                if (typeof $scope.apartmentData.title == 'undefined' ||
                    typeof $scope.apartmentData.rooms_quantity == 'undefined' ||
                    typeof $scope.apartmentData.area == 'undefined' ||
                    $scope.apartmentData.title == '' ||
                    $scope.apartmentData.rooms_quantity == null ||
                    $scope.apartmentData.area == null){

                    $ionicPopup.alert({title: "The apartment must have title, rooms quantity and area!"});
                    return;

                }

                WizardHandler.wizard().next();

            } else {

               if ($scope.selected.apartment == null){

                   $ionicPopup.alert({title: "Please choose the apartment!"});
                   return;

               }

               WizardHandler.wizard().next();

            }

        }
        
        function isStep3Valid() {

            if ($scope.newProducts.length > 0){

                _.forEach($scope.newProducts, function(product){

                    if (typeof product.title == "undefined" || product.title == ''){

                        $ionicPopup.alert({title: "Every product must have title!"});
                        return;

                    }

                    WizardHandler.wizard().next();

                });



            } else
                WizardHandler.wizard().next();

        }

        function getTitle() {

            var title = '';

            switch(WizardHandler.wizard().currentStepNumber()){

                case 1:
                    title = 'שלב 1 - הוספת דירה';
                    break;

                case 2:
                    title = 'שלב 2 - הקמת דירה';
                    break;

                case 3:
                    title = 'שלב 3 - מפרט';
                    break;

                case 4:
                    title = 'שלב 4 - תוכניות';
                    break;

                case 5:
                    title = 'שלב 5 - סיום';
                    break;

                default:
                    title = 'הקמת דירה' ;

            }

           return title;

        }

        function uploadFile(file, category_id) {

            $scope.newPlans[category_id] = file;
            console.log($scope.newPlans);
        }

        function onSave() {

            // first thing - send to the server all new products and add their ids to productsToSave array

            var promises = [];

            if ($scope.newProducts.length > 0){

                angular.forEach($scope.newProducts, function(newProduct){

                    var deferred = $q.defer();
                    promises.push(deferred.promise);

                    if (!newProduct.id) {

                        if (newProduct.file) {

                            FileService.convertFileUriToFile(newProduct.file).then(function (file) {

                                var formData = new FormData();

                                formData.append("file", file);
                                formData.append("title", newProduct.title);
                                formData.append("provider", newProduct.provider);
                                formData.append("refund", String(newProduct.refund));
                                formData.append("description", newProduct.description);
                                formData.append("product_category_id", String(newProduct.product_category_id));

                                Restangular.all('products').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                                    .then(function (data) {

                                        $scope.productsToSave.push(data.id);
                                        deferred.resolve();

                                    })

                            });

                        } else {

                            newProduct.save().then(function(data){

                                $scope.productsToSave.push(data.id);
                                deferred.resolve();

                            })

                        }

                    } else {

                        $scope.productsToSave.push(newProduct.id);
                        deferred.resolve();
                    }


                });

            }

            // now - saving plans

            $q.all(promises).then(function(){

                var promises2 = [];

                if (!angular.equals($scope.newPlans, {})){

                    _.forOwn($scope.newPlans, function(value, key){

                        var deferred2 = $q.defer();
                        promises2.push(deferred2.promise);

                        var formData = new FormData();
                        formData.append(Utils.randomString(), value);

                        Restangular.one('plan_categories', key).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                            .then(function(data) {

                                $scope.savedPlans.push(data[0]);
                                deferred2.resolve();

                            });

                        // FileService.convertFileUriToFile(value).then(function(file){
                        //
                        //     var formData = new FormData();
                        //     formData.append(Utils.randomString(), file);
                        //
                        //     Restangular.one('plan_categories', key).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)
                        //
                        //         .then(function(data) {
                        //
                        //             $scope.savedPlans.push(data[0]);
                        //             deferred2.resolve();
                        //
                        //         });
                        //
                        // })

                    });

                }

                $q.all(promises2).then(function(){

                        // making list of all apartments that must be added

                        _.forEach($scope.selected.apartments, function (apartment) {saveApartment(apartment);});

                });

                }

            );

        }

        // send all apartments to the server

        function saveApartment(key) {

            if (angular.equals({}, $scope.apartmentData.plain())) {

                $scope.apartmentData = _.merge($scope.apartmentData, $scope.selected.apartment.plain());
                $scope.apartmentData.id = null;

            }

            $scope.apartmentData.internal_id = key;

            $scope.apartmentData.save().then(saveApartmentsProducts);

        }

        // link products with apartments

        function saveApartmentsProducts(apartmentData) {

            $scope.apartmentData = apartmentData;
            $scope.savedApartments.push($scope.apartmentData);

            if ($scope.productsToSave.length > 0){

                $q.all(
                    _.map($scope.productsToSave, function (productToSave) {

                        Restangular.one('apartments', $scope.apartmentData.id).one('products', productToSave).all('relationship').post();

                    })).then(saveApartmentsPlans);

            } else
                saveApartmentsPlans();

        }

        function saveApartmentsPlans () {

            if ($scope.savedPlans.length > 0){

                _.each($scope.savedApartments, function (apartment){

                    _.each($scope.savedPlans, function(savedPlan){

                        Restangular.one('apartments', apartment.id).one('plans', savedPlan.id).all('relationship').post();

                    })

                });

            }

            WizardHandler.wizard().next();

        }

        function makeFinish() {

            WizardHandler.wizard().reset();

            Restangular.one('users', $localStorage.id).one('buildings', $stateParams.building_id).all('apartments').getList()
                .then(function(data){

                    $scope.apartments = data;

                    $scope.choice = 0;
                    $scope.selection = 1;
                    $scope.emptyApartments = findEmptyApartments();
                    $scope.selected = {
                        apartment: null,   // 2 step = apartment data
                        apartments: [], // 1 step = apartments numbers
                        products: []   // 3 step = products numbers
                    };
                    $scope.apartmentData = Restangular.restangularizeElement(Restangular.one('buildings', building.id), {}, 'apartments');   // 2 step - info about apartment
                    $scope.product_category = null;
                    $scope.newProducts = Restangular.restangularizeCollection('', [], 'products');
                    $scope.newPlans = {};
                    $scope.productsToSave = [];
                    $scope.savedApartments = [];
                    $scope.savedPlans = [];

                })

        }

        function findEmptyApartments() {

            var emptyApartments = [];

            _.forEach(_.range(1, $scope.building.apartments_quantity + 1), function (value) {
                if (_.findIndex($scope.apartments, ['internal_id', value]) === -1)
                    emptyApartments.push(value)
            });

            return Restangular.restangularizeCollection(Restangular.one('buildings', $stateParams.building_id), emptyApartments, 'apartments');
        }

        function selectProduct (x) {

            $scope.newProducts.push(Restangular.restangularizeElement('', x, 'products'));

        }

        function deselectProduct (x) {

            for (var i = 0; i < $scope.newProducts.length; i++){

                if ($scope.newProducts[i].id == x)
                    $scope.newProducts.splice(i, 1);

            }

        }

        function isProductSelected (x) {

            for (var key = 0; key < $scope.newProducts.length; key++){

                if ($scope.newProducts[key].id == x)
                    return true;

            }

        }


        function selectApartment (x) {

            $scope.selected.apartments.push(x);

        }

        function deselectApartment (x) {

            for (var i = 0; i < $scope.selected.apartments.length; i++){

                if ($scope.selected.apartments[i] == x)
                    $scope.selected.apartments.splice(i, 1);

            }

        }

        function isApartmentSelected (x) {

            for (var key = 0; key < $scope.selected.apartments.length; key++){

                if ($scope.selected.apartments[key] == x)
                    return true;

            }

        }

        function showProductsModal(product_category_id) {

            $ionicModal.fromTemplateUrl('main/templates/modal_products.html', {scope: $scope, animation: 'slide-in-up'})
                .then(function (modal) {

                    for (var j = 0; j < $scope.product_categories.plain().length; j++) {

                        if ($scope.product_categories[j].id == product_category_id) {

                            $scope.product_category = $scope.product_categories[j];

                            var newProduct = Restangular.restangularizeElement('', {}, 'products');
                            newProduct.product_category_id = $scope.product_category.id;

                            $scope.newProducts.push(newProduct);

                        }

                    }

                    $scope.modal = modal;
                    $scope.modal.show();

                });

            $scope.addProduct = function () {

                var newP = Restangular.restangularizeElement('', {}, 'products');
                newP.product_category_id = $scope.product_category.id;

                $scope.newProducts.push(newP);

            }
        }

        function closeProductsModal (){

            for (var l = 0; l < $scope.newProducts.length; l++){

                if ($scope.newProducts[l].product_category_id == $scope.product_category.id){

                    if (typeof($scope.newProducts[l].title) == 'undefined'
                        && typeof($scope.newProducts[l].provider) == 'undefined'
                        && typeof($scope.newProducts[l].description) == 'undefined'
                        && typeof($scope.newProducts[l].refund) == 'undefined'){

                        $scope.newProducts.splice(l, 1);

                    }

                }

            }

            $scope.modal.hide();

        }

    });
