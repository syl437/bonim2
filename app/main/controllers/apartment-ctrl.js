'use strict';
angular.module('main')
    .controller('ApartmentCtrl', function ($ionicSideMenuDelegate, $state, $ionicPopup, apartment, project, $log, $scope, building, Restangular) {

        $scope.project = project;
        $scope.building = building;
        $scope.apartment = apartment;
        $scope.toggleRight = function() {$ionicSideMenuDelegate.toggleRight();};

    });
