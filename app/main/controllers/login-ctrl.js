'use strict';
angular.module('main')
    .controller('LoginCtrl', function ($rootScope, $ionicPopup, $localStorage, $log, $scope, $state, Restangular) {

        // Data

        $scope.login = {phone: ''};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.login.phone == "") {

                $ionicPopup.alert({title: 'יש להזין טלפון לזיהוי'});
                return;

            }

            $localStorage.phone = $scope.login.phone;

            Restangular.all('users').one('me').get().then(authenticationSuccess, authenticationFailure);
        }

        function authenticationSuccess(data) {

            $localStorage.role = data.role;
            $rootScope.role = $localStorage.role;
            $localStorage.id = data.id;

            $rootScope.redirectUser();

        }

        function authenticationFailure() {

            delete $localStorage.phone;

        }

    });
