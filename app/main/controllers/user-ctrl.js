'use strict';
angular.module('main')
    .controller('UserCtrl', function (FileService, $ionicPopup, $state, Restangular, $scope, user) {

        $scope.user = user;

        $scope.updatePhoto = updatePhoto;
        $scope.deletePhoto = deletePhoto;

        function updatePhoto() {

            if ($scope.user.file){

                FileService.convertFileUriToFile($scope.user.file).then(function(file){

                    var formData = new FormData();
                    formData.append('file', file);

                    Restangular.one("users", $scope.user.id).all("avatar").withHttpConfig({transformRequest: angular.identity}).upload(formData).then(updateUser)

                })

            } else
                updateUser();

        }

        function updateUser() {

            $scope.user.patch().then(function(data){$scope.user = data;})

        }

        function deletePhoto() {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('files', $scope.user.files[0].id).remove().then(function (data) {$state.reload();})

            })

        }

    });
