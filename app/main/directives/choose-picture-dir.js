'use strict';
angular.module('main')
    .directive('choosePicture', function ($cordovaCamera, $ionicPopup, $log, FileService) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {

            },
            scope: {pictures: "="},
            controller: function ($scope, $element) {

                $element.on('click', function () {

                    var myPopup = $ionicPopup.show({
                        title: 'בחר/י מקור התמונה',
                        scope: $scope,
                        cssClass: 'custom-popup',
                        buttons: [
                            {
                                text: '<i class="icon ion-ios-camera"></i>', type: 'button-positive', onTap: function () {
                                addImage(1);
                            }
                            },
                            {
                                text: 'גלרייה', type: 'button-calm', onTap: function () {
                                addImage(0);
                            }
                            },
                            {
                                text: 'ביטול', type: 'button-assertive', onTap: function () {
                            }
                            }
                        ]
                    });

                });

                // Methods

                $scope.addImage = addImage;

                // Functions

                function addImage(index) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: true,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: 800,
                        targetHeight: 600,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    };

                    // Get fileUri from gallery (see Camera.DestinationType.FILE_URI in options)
                    $cordovaCamera.getPicture(options).then(function (fileUri) {

                        FileService.convertFileUriToInternalURL(fileUri).then(function(uri) {

                            if (angular.isArray($scope.pictures))
                                $scope.pictures.push({url: uri});
                            else
                                $scope.pictures = uri;

                        });

                    });
                }

            }
        };
    });
