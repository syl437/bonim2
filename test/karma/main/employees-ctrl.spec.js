'use strict';

describe('module: main, controller: EmployeesCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var EmployeesCtrl;
  beforeEach(inject(function ($controller) {
    EmployeesCtrl = $controller('EmployeesCtrl');
  }));

  it('should do something', function () {
    expect(!!EmployeesCtrl).toBe(true);
  });

});
