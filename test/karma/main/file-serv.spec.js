'use strict';

describe('module: main, service: FileService', function () {

    // load the service's module
    beforeEach(module('main'));
    // load all the templates to prevent unexpected $http requests from ui-router
    beforeEach(module('ngHtml2Js'));

    // instantiate service
    var FileService;
    beforeEach(inject(function (_FileService_) {
        FileService = _FileService_;
    }));

    it('should do something', function () {
        expect(!!FileService).toBe(true);
    });

});
