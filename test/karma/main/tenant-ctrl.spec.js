'use strict';

describe('module: main, controller: TenantCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var TenantCtrl;
  beforeEach(inject(function ($controller) {
    TenantCtrl = $controller('TenantCtrl');
  }));

  it('should do something', function () {
    expect(!!TenantCtrl).toBe(true);
  });

});
