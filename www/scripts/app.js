'use strict';
angular.module('main', [
    'ionic',
    'ngCordova',
    'ui.router',
    'ngStorage',
    'restangular',
    'mgo-angular-wizard',
    'google.places',
    'ionic.cloud',
    'ngFileUpload'
])
    .config(function ($ionicCloudProvider, $compileProvider, $stateProvider, $urlRouterProvider) {

        $ionicCloudProvider.init({
            'core': {
                'app_id': 'a4fcfa2e'
            }
        });

        // Needed to be able to display images in LiveReload mode on devices
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|file|blob|cdvfile):|data:image\//);

        // ROUTING with ui.router
        $urlRouterProvider.otherwise('/main/login');

        $stateProvider
            .state('main', {
                abstract: true,
                controller: 'MainCtrl',
                templateUrl: 'main/templates/main.html',
                url: '/main'
            })
            .state('main.login', {
                cache: false,
                url: '/login',
                views: {
                    'content': {
                        controller: 'LoginCtrl',
                        templateUrl: 'main/templates/login.html'
                    }
                }
            })

            .state('main.tenants', {
                abstract: true,
                url: '/tenants'
            })

            // List apartments for tenant
            .state('main.tenants.tenant', {
                cache: false,
                url: '/:tenant_id',
                views: {
                    'content@main': {
                        controller: 'TenantCtrl',
                        templateUrl: 'main/templates/tenant.html'
                    }
                },
                resolve: {tenant_apartments: function(Restangular, $stateParams){ return Restangular.one('users', $stateParams.tenant_id).all('apartments').getList()}}
            })

            // Profile
            .state('main.user', {
                cache: false,
                url: '/user',
                views: {
                    'content@main': {
                        controller: 'UserCtrl',
                        templateUrl: 'main/templates/user.html'
                    }
                },
                resolve: {user: function(Restangular, $localStorage){ return Restangular.one('users', $localStorage.id).get()}}
            })

            // List All Projects
            .state('main.projects', {
                cache: false,
                resolve: {projects: function (Restangular, $localStorage) {return Restangular.one('users', $localStorage.id).all('projects').getList();}},
                url: '/projects',
                views: {
                    'content': {
                        controller: 'ProjectsCtrl',
                        templateUrl: 'main/templates/projects.html'
                    }
                }
            })

            .state('main.projects_list', {
                cache: false,
                resolve: {projects: function (Restangular) {return Restangular.one('users', '1').all('projects').getList();}},
                url: '/projects_list',
                views: {
                    'content': {
                        controller: 'ProjectsCtrl',
                        templateUrl: 'main/templates/projects.html'
                    }
                }
            })

            // Create New Projects
            .state('main.projects.create', {
                cache: false,
                resolve: {project: function (Restangular) {return Restangular.restangularizeElement(null, {name: '', address: '', buildings_quantity: '', status: '', buildings : []}, 'projects');}},
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'ProjectCreateCtrl',
                        templateUrl: 'main/templates/project_create.html'
                    }
                }
            })

            // Edit Existing Project
            .state('main.projects.project', {
                cache: false,
                resolve: {project: function ($stateParams, Restangular) {return Restangular.one('projects', $stateParams.project_id).get();}},
                url: '/:project_id',
                views: {
                    'content@main': {
                        controller: 'ProjectCtrl',
                        templateUrl: 'main/templates/project.html'
                    }
                }
            })

            // List All Buildings Belonging to Project
            .state('main.projects.project.buildings', {
                cache: false,
                resolve: {buildings: function ($stateParams, Restangular, $localStorage) {return Restangular.one('users', $localStorage.id).one('projects', $stateParams.project_id).all('buildings').getList();}},
                url: '/buildings',
                views: {
                    'content@main': {
                        controller: 'BuildingsCtrl',
                        templateUrl: 'main/templates/buildings.html'
                    }
                }
            })

            // Create New Buildings Belonging To Project
            .state('main.projects.project.buildings.create', {
                cache: false,
                resolve: {building: function ($stateParams, Restangular) {return Restangular.restangularizeElement(Restangular.one('projects', $stateParams.project_id), {}, 'buildings');}},
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'BuildingCtrl',
                        templateUrl: 'main/templates/building.html'
                    }
                }
            })

            .state('main.buildings', {
                abstract: true,
                url: '/buildings'
            })


            // Edit Existing Building
            .state('main.buildings.building', {
                cache: false,
                resolve: {building: function ($stateParams, Restangular) {return Restangular.one('buildings', $stateParams.building_id).get();}},
                url: '/:building_id',
                views: {
                    'content@main': {
                        controller: 'BuildingCtrl',
                        templateUrl: 'main/templates/building.html'
                    }
                }
            })

            // List All Apartments Belonging To Building
            .state('main.buildings.building.apartments', {
                cache: false,
                resolve: {
                    apartments: function ($stateParams, $localStorage, Restangular) {
                        return Restangular.one('users', $localStorage.id).one('buildings', $stateParams.building_id).all('apartments').getList();
                    },
                    product_categories: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).all('product_categories').getList();
                    },
                    plan_categories: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).all('plan_categories').getList();
                    },
                    project: function ($stateParams, building, Restangular) {
                        return Restangular.one('projects', building.project_id).get();
                    }
                },
                url: '/apartments',
                views: {
                    'content@main': {
                        controller: 'ApartmentsCtrl',
                        templateUrl: 'main/templates/apartments.html'
                    }
                }
            })

            // Create New Apartment Belonging To Building
            .state('main.buildings.building.apartments.create', {
                cache: false,
                url: '/create',
                views: {
                    'content@main': {
                        controller: 'ApartmentCreateCtrl',
                        templateUrl: 'main/templates/apartment_create.html'
                    }
                }
            })

            // Look into the apartment
            .state('main.buildings.building.apartments.apartment', {
                cache: false,
                resolve: {apartment: function ($stateParams, Restangular) {return Restangular.one('apartments', $stateParams.apartment_id).get();}},
                url: '/:apartment_id',
                views: {
                    'content@main': {
                        controller: 'ApartmentCtrl',
                        templateUrl: 'main/templates/apartment.html'
                    }
                }
            })

            // Look into the apartment
            .state('main.buildings.building.apartments.apartment_edit', {
                cache: false,
                resolve: {apartment: function ($stateParams, Restangular) {return Restangular.one('apartments', $stateParams.apartment_id).get();}},
                url: '/:apartment_id/edit',
                views: {
                    'content@main': {
                        controller: 'ApartmentEditCtrl',
                        templateUrl: 'main/templates/apartment_edit.html'
                    }
                }
            })

            // Look into the apartment products
            .state('main.buildings.building.apartments.apartment.products', {
                cache: false,
                url: '/products',
                views: {
                    'content@main': {
                        controller: 'ProductsCtrl',
                        templateUrl: 'main/templates/products.html'
                    }
                }
            })

            // Look into the apartment plans
            .state('main.buildings.building.apartments.apartment.plans', {
                cache: false,
                url: '/plans',
                views: {
                    'content@main': {
                        controller: 'PlansCtrl',
                        templateUrl: 'main/templates/plans.html'
                    }
                }
            })

            // Look into the apartment defects
            .state('main.buildings.building.apartments.apartment.defects', {
                cache: false,
                url: '/defects',
                views: {
                    'content@main': {
                        controller: 'DefectsCtrl',
                        templateUrl: 'main/templates/defects.html'
                    }
                }
            })


            // Create new defect
            .state('main.buildings.building.apartments.apartment.defects.create', {
                cache: false,
                url: '/defect_create',
                views: {
                    'content@main': {
                        controller: 'DefectCreateCtrl',
                        templateUrl: 'main/templates/defect_create.html'
                    }
                }
            })


            // Look into the apartment defect
            .state('main.buildings.building.apartments.apartment.defects.defect', {
                cache: false,
                url: '/:defect_id',
                views: {
                    'content@main': {
                        controller: 'DefectCtrl',
                        templateUrl: 'main/templates/defect.html'
                    }
                },
                resolve: {defect : function($stateParams, Restangular){return Restangular.one('defects', $stateParams.defect_id).get()}}
            })

            .state('main.buildings.building.apartments.apartment.connections', {
                cache: false,
                url: '/connections',
                views: {
                    'content@main': {
                        controller: 'ConnectionsCtrl',
                        templateUrl: 'main/templates/connections.html'
                    }
                }
            })

            // Look into the apartment gallery
            .state('main.buildings.building.apartments.apartment.gallery', {
                cache: false,
                url: '/gallery',
                views: {
                    'content@main': {
                        controller: 'GalleryCtrl',
                        templateUrl: 'main/templates/gallery.html'
                    }
                }
            })

            // Look into the apartment chat
            .state('main.buildings.building.apartments.apartment.messages', {
                cache: false,
                url: '/messages',
                views: {
                    'content@main': {
                        controller: 'MessagesCtrl',
                        templateUrl: 'main/templates/messages.html'
                    }
                }
            })

            ;

    })

    .run(function ($ionicPopup, $ionicLoading, $ionicHistory, $localStorage, $log, $rootScope, $state, Config, Restangular) {

        $rootScope.options = {loop: true, effect: 'slide', autoplay: false};
        $rootScope.enableBack = false;
        $rootScope.enableMenu = false;
        $rootScope.role = $localStorage.role;
        $rootScope.goBack = function(){$ionicHistory.goBack();};

        $rootScope.redirectUser = function () {

            if ($localStorage.role != 'tenant'){

                $state.go('main.projects');

            } else {
                console.log('here');
                // return;
                Restangular.one('users', $localStorage.id).all('apartments').getList()
                    .then(function(response){

                        if (response.length == 0){

                            $ionicPopup.alert({title: 'There is no apartments for this user!'});
                            delete $localStorage.id;
                            $state.go('main.login');

                        }

                        if (response.length == 1)
                            $state.go('main.buildings.building.apartments.apartment', {building_id: response[0].apartment.building_id, apartment_id: response[0].apartment.id}, {reload: true});
                        else
                            $state.go('main.tenants.tenant', {tenant_id : $localStorage.id}, {reload: true});

                    });

            }
        };

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {

            if($localStorage.role != 'tenant')
                $state.go('main.projects');
            else
                console.log('error!!!');

        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {

            if (toState.name === 'main.projects' ||
                toState.name === 'main.tenants.tenant' ||
                toState.name === 'main.user' ||
                toState.name === 'main.buildings.building.apartments.create'){

                $rootScope.enableBack = false;
                $rootScope.enableMenu = true;

            } else {

                $rootScope.enableBack = true;
                $rootScope.enableMenu = false;

            }

        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

            if (toState.name === "main.login" && $localStorage.id) {

                event.preventDefault();
                $rootScope.redirectUser();

            }

        });

        // See app/main/constants/env-dev.json and app/main/constants/env-prod.json
        Restangular.setBaseUrl(Config.ENV.SERVER_URL);

        // Attach phone number as token for each API request
        Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers) {

            return {
                headers: _.merge({Authorization: 'Bearer ' + $localStorage.phone}, headers)
            };

        });

        Restangular.addRequestInterceptor(function(element, operation, what, url){

            $ionicLoading.show({

                template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'

            });

            return element;

        });

        // Handle response
        Restangular.addResponseInterceptor(function (data, operation, what, url) {

            if (data.data) {
                $log.log(url, Restangular.copy(data.data).plain());
            }

            var extractedData;

            if (operation === 'getList') {

                extractedData = data.data;

            } else {

                extractedData = data.data;

            }

            $ionicLoading.hide();

            return extractedData;

        });

        Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {

            if (response){

                $ionicLoading.hide();

                switch (response.data.code){

                    case 401:
                        $ionicPopup.alert({title: "מספר טלפון לא נכון"});
                        break;

                    case 404:
                        $ionicPopup.alert({title: 'There is no apartments for this user!'});
                        delete $localStorage.id;
                        $state.go('main.login');
                        break;

                    default:
                        $ionicPopup.alert({title: 'Error happened!'});
                        break;

                }

            }

            return response;
        });

        // Add custom method upload for images so it will be possible to call

        Restangular.addElementTransformer('images', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('files', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('products', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('plans', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('defects', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('comments', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });

        Restangular.addElementTransformer('images', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });


        Restangular.addElementTransformer('avatar', true, function (image) {

            image.addRestangularMethod('upload', 'post', undefined, undefined, {'Content-Type': undefined});

            return image;
        });
    });

'use strict';
angular.module('main')
    .service('Utils', function ($ionicPopup) {

        // Public

        this.randomString = randomString;
        this.isAnyFieldEmpty = isAnyFieldEmpty;

        // Private

        function randomString() {

            return (Math.random() * 1e32).toString(36);

        }

        function isAnyFieldEmpty(x){

            var keys = Object.getOwnPropertyNames(x);

            for (var key in keys){

                if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null){

                    $ionicPopup.alert({title: "נא למלא את כל השדות"});

                    return true;

                }

            }

            return false;

        }

    });

'use strict';
angular.module('main')
    .service('FileService', function ($log, $q) {

        // Public

        this.convertFileUriToFile = convertFileUriToFile;
        this.convertFileUrisToFiles = convertFileUrisToFiles;
        this.convertFileUriToInternalURL = convertFileUriToInternalURL;

        // Private

        // Converts File URI to proper Blob that will be sent to
        // server within FormData bag. We need it just because
        // iOS doesn't work good with FormData and this is the only
        // way to implement it
        function convertFileUriToFile(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                // .file method is accepting a callback that receives file (which is instance of
                // File object that will be converted to Blob)
                fileEntry.file(function (file) {

                    // initialize FileReader
                    var reader = new FileReader();

                    // attach callback that will run upon FileReader finishes reading file
                    reader.onloadend = function (evt) {

                        deferred.resolve(new Blob([evt.target.result]), fileUri);

                    };

                    reader.onerror = function () {

                        deferred.reject();

                    };

                    // read file as array buffer
                    reader.readAsArrayBuffer(file);

                });
            }

            return deferred.promise;

        }

        function convertFileUrisToFiles(fileUris) {

            return $q.all(fileUris.map(function (file) {

                return convertFileUriToFile(file.url || file);

            }));
        }

        // Converts File URI to proper Internal URL that will be used
        // to display local device images while being in LiveReload mode
        function convertFileUriToInternalURL(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved, onFileResolveError);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                deferred.resolve(fileEntry.toInternalURL());
            }

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolveError() {

                deferred.reject();
            }

            return deferred.promise;
        }

    });

'use strict';
angular.module('main')
    .directive('choosePicture', function ($cordovaCamera, $ionicPopup, $log, FileService) {
        return {
            restrict: 'AE',
            link: function (scope, element, attrs) {

            },
            scope: {pictures: "="},
            controller: function ($scope, $element) {

                $element.on('click', function () {

                    var myPopup = $ionicPopup.show({
                        title: 'בחר/י מקור התמונה',
                        scope: $scope,
                        cssClass: 'custom-popup',
                        buttons: [
                            {
                                text: '<i class="icon ion-ios-camera"></i>', type: 'button-positive', onTap: function () {
                                addImage(1);
                            }
                            },
                            {
                                text: 'גלרייה', type: 'button-calm', onTap: function () {
                                addImage(0);
                            }
                            },
                            {
                                text: 'ביטול', type: 'button-assertive', onTap: function () {
                            }
                            }
                        ]
                    });

                });

                // Methods

                $scope.addImage = addImage;

                // Functions

                function addImage(index) {
                    var options = {
                        quality: 75,
                        destinationType: Camera.DestinationType.FILE_URI,
                        sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: true,
                        encodingType: Camera.EncodingType.JPEG,
                        targetWidth: 800,
                        targetHeight: 600,
                        popoverOptions: CameraPopoverOptions,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    };

                    // Get fileUri from gallery (see Camera.DestinationType.FILE_URI in options)
                    $cordovaCamera.getPicture(options).then(function (fileUri) {

                        FileService.convertFileUriToInternalURL(fileUri).then(function(uri) {

                            if (angular.isArray($scope.pictures))
                                $scope.pictures.push({url: uri});
                            else
                                $scope.pictures = uri;

                        });

                    });
                }

            }
        };
    });

'use strict';
angular.module('main')
    .controller('UserCtrl', function (FileService, $ionicPopup, $state, Restangular, $scope, user) {

        $scope.user = user;

        $scope.updatePhoto = updatePhoto;
        $scope.deletePhoto = deletePhoto;

        function updatePhoto() {

            if ($scope.user.file){

                FileService.convertFileUriToFile($scope.user.file).then(function(file){

                    var formData = new FormData();
                    formData.append('file', file);

                    Restangular.one("users", $scope.user.id).all("avatar").withHttpConfig({transformRequest: angular.identity}).upload(formData).then(updateUser)

                })

            } else
                updateUser();

        }

        function updateUser() {

            $scope.user.patch().then(function(data){$scope.user = data;})

        }

        function deletePhoto() {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('files', $scope.user.files[0].id).remove().then(function (data) {$state.reload();})

            })

        }

    });

'use strict';
angular.module('main')
    .controller('TenantCtrl', function ($state, $scope, tenant_apartments) {

        $scope.tenant_apartments = tenant_apartments;

    });

'use strict';
angular.module('main')
    .controller('ProjectsCtrl', function ($localStorage, Restangular, $state, $log, $scope, projects) {

        $scope.projects = projects;

        $scope.moveIntoProject = moveIntoProject;

        function moveIntoProject(proj) {

            if ($localStorage.role != 'tenant'){

                if (proj.buildings_quantity == 1){

                    Restangular.one('users', $localStorage.id).one('projects', proj.id).all('buildings').getList()

                        .then(function(data){

                            $state.go("main.buildings.building.apartments", {building_id : data[0].id});

                        });

                }

                else
                    $state.go("main.projects.project.buildings", {project_id : proj.id});

            }

        }

    });

'use strict';
angular.module('main')
    .controller('ProjectCtrl', function ($ionicPopup, $localStorage, $log, $q, $scope, $state, FileService, Restangular, Utils, project) {

        // Data

        $scope.project = project;
        $scope.localImages = [];
        $scope.autocompleteOptions = {componentRestrictions: {country: 'il'}};

        // Methods

        $scope.saveProject = saveProject;
        $scope.deleteProject = deleteProject;
        $scope.addUser = addUser;
        $scope.deleteConnection = deleteConnection;
        $scope.deleteImage = deleteImage;

        // Functions

        function saveProject() {

            // Check for empty fields or any other mistakes

            if (Utils.isAnyFieldEmpty($scope.project.plain()))
                return;

            if ($scope.project.address.formatted_address)
                $scope.project.address = $scope.project.address.formatted_address;


            $scope.project.put().then(onProjectSaved);

        }

        function onProjectSaved(project) {

            if ($scope.localImages.length > 0){

                FileService.convertFileUrisToFiles($scope.localImages).then(function (files) {

                    if (files.length == 0) {

                        $state.go('main.projects', null, {reload: true});
                        return;

                    }

                    var formData = new FormData();

                    files.map(function (file) {

                        formData.append(Utils.randomString(), file);

                    });

                    formData.append('type', 'project');
                    formData.append('id', project.id);

                    Restangular.all('files').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(onPicturesSaved);

                });

            } else
                onPicturesSaved();

        }

        function onPicturesSaved() {

            $ionicPopup.alert({title: 'Successfully updated!'});
            $state.go('main.projects', null, {reload: true});

        }

        function deleteProject() {

            $scope.project.remove().then(onProjectDeleted, onProjectDeleteError);

        }

        function onProjectDeleted() {

            $state.go('main.projects', null, {reload: true});

        }

        function onProjectDeleteError() {

        }

        function addUser() {

            navigator.contacts.pickContact(function (contact) {

                var newUser = Restangular.restangularizeElement('', {}, 'users');

                newUser.phone = contact.phoneNumbers[0].value;
                newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
                newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
                newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
                newUser.description = contact.note ? contact.note : newUser.phone;
                newUser.type_role = 'employee';
                newUser.type = 'project';
                newUser.type_id = $scope.project.id;

                Restangular.all('users').customPOST(newUser, 'permissions').then(function (data) {

                    $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                    $state.reload();

                });

            }, function (err) {});

        }

        function deleteConnection(person) {

            var data = {};
            data.user_id = person;
            data.type = 'project';
            data.type_id = $scope.project.id;

            Restangular.all('users').customPOST(data, 'prohibitions').then(function () {

                $state.reload();

            });

        }

        function deleteImage(file) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('files', file.id).remove().then(function (data) {$state.reload();})

            })

        }

    });
'use strict';
angular.module('main')
    .controller('ProjectCreateCtrl', function ($ionicPopup, $localStorage, $log, $q, $scope, $state, FileService, Restangular, Utils, project) {

        // Data

        $scope.project = project;
        $scope.localImages = [];
        $scope.autocompleteOptions = {componentRestrictions: {country: 'il'}};

        // Methods

        $scope.saveProject = saveProject;

        // Functions

        function saveProject() {

            // Check for empty fields or any other mistakes

            if (Utils.isAnyFieldEmpty($scope.project.plain()))
                return;


            if (!$scope.project.buildings.length){

                $ionicPopup.alert({title: "Please add at least one building!"});
                return;

            } else {

                for (var i = 0; i < $scope.project.buildings.length; i++){

                    if (typeof $scope.project.buildings[i].apartments_quantity === "undefined" || $scope.project.buildings[i].apartments_quantity === null){

                        $ionicPopup.alert({title: "Every building must have apartments number!"});
                        return;

                    }

                }

            }

            if ($scope.localImages.length == 0){

                $ionicPopup.alert({title: "Please add at least one image!"});
                return;

            }

            if ($scope.project.buildings.length != $scope.project.buildings_quantity){

                $ionicPopup.alert({title: "Please check buildings once again, something is empty!"});
                return;

            }

            if ($scope.project.address.formatted_address)
                $scope.project.address = $scope.project.address.formatted_address;

            for (var j = 0; j < $scope.project.buildings.length; j++){

                if (typeof $scope.project.buildings[j].name === "undefined" || $scope.project.buildings[j].name === '')
                    $scope.project.buildings[j].name = " בניין " + j;

            }

            $scope.project.save().then(onProjectSaved);

        }

        function onProjectSaved(project) {

            FileService.convertFileUrisToFiles($scope.localImages).then(function (files) {

                if (files.length == 0) {

                    $state.go('main.projects', null, {reload: true});
                    return;

                }

                var formData = new FormData();

                files.map(function (file) {

                    formData.append(Utils.randomString(), file);

                });

                formData.append('type', 'project');
                formData.append('id', project.id);

                Restangular.all('files').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(onPicturesSaved);

            });

        }

        function onPicturesSaved() {

            $state.go('main.projects', null, {reload: true});

        }

    });

'use strict';
angular.module('main')
.controller('ProductsCtrl', function ($ionicModal, $log, $scope, apartment) {

    $scope.apartment = apartment;
    $scope.product_category = null;

    $scope.showProductsModal = showProductsModal;
    $scope.closeProductsModal = closeProductsModal;

    function showProductsModal(product_category_id) {

        $ionicModal.fromTemplateUrl('main/templates/modal_show_products.html', {scope: $scope, animation: 'slide-in-up'})
            .then(function (modal) {

                for (var j = 0; j < $scope.apartment.product_categories.length; j++) {

                    if ($scope.apartment.product_categories[j].id == product_category_id)
                        $scope.product_category = $scope.apartment.product_categories[j];

                }

                $scope.modal = modal;
                $scope.modal.show();

            });
    }

    function closeProductsModal (){

        $scope.modal.hide();

    }

});

'use strict';
angular.module('main')
    .controller('PlansCtrl', function ($log, $scope, apartment) {

        $scope.apartment = apartment;

        $scope.openPlan = openPlan;
        $scope.sharePlan = sharePlan;

        function openPlan(x) {cordova.InAppBrowser.open(x, '_blank', 'location=yes');}

        function sharePlan(url) {

            var options = {subject: 'תוכניות', url: url};

            var onSuccess = function(result) {console.log("success");};

            var onError = function(msg) {console.log("fail");};

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);

        }

    });

'use strict';
angular.module('main')
    .controller('MessagesCtrl', function ($scope, apartment, Restangular) {

        $scope.apartment = apartment;
        $scope.message = Restangular.restangularizeElement(Restangular.one('apartments', $scope.apartment.id), {}, 'messages');

        $scope.sendMessage = sendMessage;

        function sendMessage() {

            $scope.message.save().then(function(){

                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){

                    $scope.apartment = data;
                    $scope.message = Restangular.restangularizeElement(Restangular.one('apartments', $scope.apartment.id), {}, 'messages');


                });

            })

        }

    });

'use strict';
angular.module('main')
    .controller('MainCtrl', function ($state, $localStorage, $scope) {

        $scope.logout = logout;

        function logout () {

           delete $localStorage.phone;
           delete $localStorage.role;
           delete $localStorage.id;
           $state.go('main.login');

        }

    });

'use strict';
angular.module('main')
    .controller('LoginCtrl', function ($rootScope, $ionicPopup, $localStorage, $log, $scope, $state, Restangular) {

        // Data

        $scope.login = {phone: ''};

        // Methods

        $scope.authenticate = authenticate;

        // Functions
        function authenticate() {

            if ($scope.login.phone == "") {

                $ionicPopup.alert({title: 'יש להזין טלפון לזיהוי'});
                return;

            }

            $localStorage.phone = $scope.login.phone;

            Restangular.all('users').one('me').get().then(authenticationSuccess, authenticationFailure);
        }

        function authenticationSuccess(data) {

            $localStorage.role = data.role;
            $rootScope.role = $localStorage.role;
            $localStorage.id = data.id;

            $rootScope.redirectUser();

        }

        function authenticationFailure() {

            delete $localStorage.phone;

        }

    });

'use strict';
angular.module('main')
    .controller('GalleryCtrl', function (Upload, $ionicSlideBoxDelegate, $ionicModal, $cordovaCamera, $ionicPopup, $state, FileService, Restangular, $scope, apartment) {

        $scope.apartment = apartment;
        $scope.image = null;
        $scope.picFile = '';

        $ionicModal.fromTemplateUrl('main/templates/modal_image.html', {scope: $scope, animation: 'slide-in-up'}).then(function(modal) {$scope.modal = modal; });

        $scope.addNewImage = addNewImage;
        $scope.closeImageModal = function closeImageModal() {$scope.modal.hide(); $scope.image = null;};
        $scope.deleteImage = deleteImage;

        $scope.goToSlide = function(image) {

            $scope.image = image;
            $scope.modal.show();

        };

        function addNewImage() {

            $ionicPopup.show({
                title: 'בחר/י מקור התמונה',
                scope: $scope,
                cssClass: 'custom-popup',
                buttons: [
                    {text: 'מצלמה', type: 'button-positive', onTap: function () {sendImage(1);}},
                    {text: 'גלרייה', type: 'button-calm', onTap: function () {sendImage(0);}},
                    {text: 'ביטול', type: 'button-assertive', onTap: function () {}}
                ]
            });
            
        }

        function sendImage(index) {

            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: 1,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 600,
                targetHeight: 600,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };

            // Get fileUri from gallery (see Camera.DestinationType.FILE_URI in options)
            $cordovaCamera.getPicture(options).then(function (fileUri) {

                FileService.convertFileUriToInternalURL(fileUri).then(function(uri) {

                    FileService.convertFileUriToFile(uri).then(function(file){

                        var formData = new FormData();
                        formData.append('file', file);

                        Restangular.one('apartments', $scope.apartment.id).all('images').withHttpConfig({transformRequest: angular.identity}).upload(formData)
                            .then(function(){

                                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){$scope.apartment = data;})


                            })

                    })

                });

            });

        }

        function deleteImage(image) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('images', image.id).remove().then(function (data) {console.log(data);$state.reload();})

            })

        }

    });

'use strict';
angular.module('main')
.controller('DefectsCtrl', function ($state, $ionicPopup, $scope, $log, $filter, apartment, Restangular) {

    $scope.apartment = apartment;
    $scope.tab = 0;
    $scope.propertyName = 'formatted_created_at';
    $scope.reverse = false;

    $scope.sortBy = sortBy;
    $scope.updateStatus = updateStatus;

    function sortBy (propertyName) {

        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : true;
        $scope.propertyName = propertyName;
        $scope.apartment.defects = $filter('orderBy')($scope.apartment.defects, $scope.propertyName, $scope.reverse);

    }

    function updateStatus (defect, status) {

        defect = Restangular.restangularizeElement('', defect, 'defects');
        defect.closed = status;
        defect.patch().then(function(data){

            $ionicPopup.alert({title: "Updated!"}).then(function(){

                // $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});
                $state.reload();

            })

        });

    }

});

'use strict';
angular.module('main')
    .controller('DefectCtrl', function ($localStorage, Restangular, FileService, $state, $ionicPopup, $scope, defect, apartment) {

        $scope.defect = defect;
        $scope.apartment = apartment;
        $scope.newComment = {};

        $scope.changeStatus = changeStatus;
        $scope.deleteTicket = deleteTicket;
        $scope.addComment = addComment;
        $scope.deleteImage = deleteImage;

        function changeStatus (status) {

            $scope.defect.closed = status;
            $scope.defect.patch().then(function(){

                var title = status == 1 ? "Closed!" : "Opened!";

                $ionicPopup.alert({title: title}).then(function(){

                    $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

                });

            });

        }

        function deleteTicket() {

            $scope.defect.remove().then(function(){

                $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

            })

        }

        function deleteImage() {

            delete $scope.newComment.file;

        }

        function addComment() {

            var formData = new FormData();
            formData.append('defect_id', $scope.defect.id);
            formData.append('user_id', $localStorage.id);
            formData.append('content', $scope.newComment.content);

            if ($scope.newComment.file){

                FileService.convertFileUriToFile($scope.newComment.file).then(function(file){

                    formData.append('file', file);

                    Restangular.one('defects', $scope.defect.id).all('comments').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(function (data) {$state.reload();})

                })


            } else
                Restangular.one('defects', $scope.defect.id).all('comments').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(function (data) {$state.reload();})

        }

    });

'use strict';
angular.module('main')
.controller('DefectCreateCtrl', function ($localStorage, $ionicPopup, $state, FileService, $log, $scope, Restangular, apartment, building) {

    $scope.apartment = apartment;
    $scope.building = building;
    $scope.newDefect = Restangular.restangularizeElement('', {}, 'defects');

    $scope.makeNewTicket = makeNewTicket;

    function makeNewTicket () {

        var formData = new FormData();
        formData.append('apartment_id', $scope.apartment.id);
        formData.append('user_id', $localStorage.id);
        formData.append('location', $scope.newDefect.location);
        formData.append('description', $scope.newDefect.description);

        if ($scope.newDefect.file){

            FileService.convertFileUriToFile($scope.newDefect.file).then(function(file){

                formData.append('file', file);

                Restangular.one('defect_categories', $scope.newDefect.category).all('defects').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(success)

            })


        } else
            Restangular.one('defect_categories', $scope.newDefect.category).all('defects').withHttpConfig({transformRequest: angular.identity}).upload(formData).then(success)

    }

    function success() {

        Restangular.one('apartments', $scope.apartment.id).get().then(function(){

            $state.go('main.buildings.building.apartments.apartment.defects', {building: apartment.building_id, apartment : apartment.id}, {reload: true});

        })

    }

});

'use strict';
angular.module('main')
.controller('ConnectionsCtrl', function ($ionicPopup, $state, Restangular, $scope, apartment) {

    $scope.apartment = apartment;

    $scope.deleteConnection = deleteConnection;
    $scope.addUser = addUser;

    function addUser () {

        navigator.contacts.pickContact(function(contact){

            var newUser = Restangular.restangularizeElement('', {}, 'users');

            newUser.phone = contact.phoneNumbers[0].value;
            newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
            newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
            newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
            newUser.description = contact.note ? contact.note : newUser.phone;
            newUser.type_role = 'tenant';
            newUser.type = 'apartment';
            newUser.type_id = $scope.apartment.id;

            Restangular.all('users').customPOST(newUser, 'permissions').then(function(data){

                $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                $state.reload();

            });

        },function(err){});

    }


    function deleteConnection(person) {

        var data = {};
        data.user_id = person;
        data.type = 'apartment';
        data.type_id = $scope.apartment.id;

        Restangular.all('users').customPOST(data, 'prohibitions').then(function(data){

            $state.reload();

        });

    }

});

'use strict';
angular.module('main')
    .controller('BuildingsCtrl', function ($log, $scope, project, buildings) {

        // Data

        $scope.buildings = buildings;
        $scope.project = project;

    });

'use strict';
angular.module('main')
    .controller('BuildingCtrl', function (Restangular, $ionicPopup, $log, $scope, $state, building) {

        // Data

        $scope.building = building;

        // Methods

        $scope.saveBuilding = saveBuilding;
        $scope.addUser = addUser;
        $scope.deleteBuilding = deleteBuilding;
        $scope.deleteConnection = deleteConnection;

        // Functions

        function saveBuilding() {

            $scope.building.save().then(onBuildingSaved, onBuildingSaveError);

        }

        function onBuildingSaved(updatedBuilding) {

            $scope.building = updatedBuilding;

            $state.go('main.projects.project.buildings', {project_id: $scope.building.project_id}, {reload: true});
        }

        function onBuildingSaveError() {

        }

        function deleteBuilding() {

            $scope.building.remove().then(function(){
                $state.go('main.projects.project.buildings', {project_id: $scope.building.project_id}, {reload: true})
            })

        }

        function addUser (x) {

            navigator.contacts.pickContact(function(contact){

                var newUser = Restangular.restangularizeElement('', {}, 'users');

                newUser.phone = contact.phoneNumbers[0].value;
                newUser.phone = newUser.phone.replace(/[^0-9]+/g, '');
                newUser.name = contact.name.formatted ? contact.name.formatted : newUser.phone;
                newUser.email = contact.emails ? contact.emails[0].value : newUser.phone;
                newUser.description = contact.note ? contact.note : newUser.phone;
                newUser.type_role = 'employee';
                newUser.type = 'building';
                newUser.type_id = $scope.building.id;

                Restangular.all('users').customPOST(newUser, 'permissions').then(function(data){

                    $ionicPopup.alert({title: "You have successfully added contact " + data.name});
                    $state.reload();

                });

            },function(err){});

        }

        function deleteConnection(person) {

            var data = {};
            data.user_id = person;
            data.type = 'building';
            data.type_id = $scope.building.id;

            Restangular.all('users').customPOST(data, 'prohibitions').then(function(data){

                $state.reload();

            });

        }


    });

'use strict';
angular.module('main')
    .controller('ApartmentsCtrl', function ($ionicPopup, $state, Restangular, $log, $scope, apartments, building, project) {

        // Data

        $scope.apartments = apartments;
        $scope.building = building;
        $scope.project = project;

        // Methods
        $scope.deleteApartment = deleteApartment;

        // Functions

        function deleteApartment(id) {

            var confirmPopup = $ionicPopup.confirm({title: 'Are you sure?'});

            confirmPopup.then(function(res) {

                if(res)
                    Restangular.one('apartments', id).remove().then(function (data) {$state.reload();})

            })
        }
    });

'use strict';
angular.module('main')
    .controller('ApartmentCtrl', function ($ionicSideMenuDelegate, $state, $ionicPopup, apartment, project, $log, $scope, building, Restangular) {

        $scope.project = project;
        $scope.building = building;
        $scope.apartment = apartment;
        $scope.toggleRight = function() {$ionicSideMenuDelegate.toggleRight();};

    });

'use strict';
angular.module('main')
    .controller('ApartmentCreateCtrl', function ($timeout, $ionicScrollDelegate, $ionicPopup, $localStorage, WizardHandler, $ionicModal, $log, $q, $scope, $state, $stateParams, FileService, Restangular, Utils, apartments, building, plan_categories, product_categories) {

        // Data

        $scope.apartments = apartments;
        $scope.building = building;
        $scope.choice = 0;
        $scope.selection = 1;
        $scope.emptyApartments = findEmptyApartments();
        $scope.product_categories = product_categories;
        $scope.plan_categories = plan_categories;
        $scope.selected = {
            apartment: null,   // 2 step = apartment data
            apartments: [], // 1 step = apartments numbers
            products: []   // 3 step = products numbers
        };
        $scope.apartmentData = Restangular.restangularizeElement(Restangular.one('buildings', building.id), {}, 'apartments');   // 2 step - info about apartment
        $scope.product_category = null;
        $scope.newProducts = Restangular.restangularizeCollection('', [], 'products');
        $scope.newPlans = {};
        $scope.productsToSave = [];
        $scope.savedApartments = [];
        $scope.savedPlans = [];

        // Methods

        $scope.onSave = onSave;
        $scope.makeFinish = makeFinish;
        $scope.showProductsModal = showProductsModal;
        $scope.closeProductsModal = closeProductsModal;
        $scope.getStep = function getStep (){return WizardHandler.wizard().currentStepNumber();};
        $scope.stepBack = function stepBack(){return WizardHandler.wizard().previous()};
        $scope.changeChoice = function changeChoice(x){$scope.choice = x;};
        $scope.changeSelection = changeSelection;
        $scope.selectApartment = selectApartment;
        $scope.deselectApartment = deselectApartment;
        $scope.isApartmentSelected = isApartmentSelected;
        $scope.selectProduct = selectProduct;
        $scope.deselectProduct = deselectProduct;
        $scope.isProductSelected = isProductSelected;
        $scope.isStep1Valid = isStep1Valid;
        $scope.isStep2Valid = isStep2Valid;
        $scope.isStep3Valid = isStep3Valid;
        $scope.getTitle = getTitle;
        $scope.uploadFile = uploadFile;

        // Functions

        function changeSelection(x){

            $scope.selection = x;
            $ionicScrollDelegate.scrollTop();

        }

        function isStep1Valid() {

           if ($scope.selected.apartments.length > 0)
               return true;

           return false;

        }

        function isStep2Valid(index) {

            if (index == 0){

                if (typeof $scope.apartmentData.title == 'undefined' ||
                    typeof $scope.apartmentData.rooms_quantity == 'undefined' ||
                    typeof $scope.apartmentData.area == 'undefined' ||
                    $scope.apartmentData.title == '' ||
                    $scope.apartmentData.rooms_quantity == null ||
                    $scope.apartmentData.area == null){

                    $ionicPopup.alert({title: "The apartment must have title, rooms quantity and area!"});
                    return;

                }

                WizardHandler.wizard().next();

            } else {

               if ($scope.selected.apartment == null){

                   $ionicPopup.alert({title: "Please choose the apartment!"});
                   return;

               }

               WizardHandler.wizard().next();

            }

        }
        
        function isStep3Valid() {

            if ($scope.newProducts.length > 0){

                _.forEach($scope.newProducts, function(product){

                    if (typeof product.title == "undefined" || product.title == ''){

                        $ionicPopup.alert({title: "Every product must have title!"});
                        return;

                    }

                    WizardHandler.wizard().next();

                });



            } else
                WizardHandler.wizard().next();

        }

        function getTitle() {

            var title = '';

            switch(WizardHandler.wizard().currentStepNumber()){

                case 1:
                    title = 'שלב 1 - הוספת דירה';
                    break;

                case 2:
                    title = 'שלב 2 - הקמת דירה';
                    break;

                case 3:
                    title = 'שלב 3 - מפרט';
                    break;

                case 4:
                    title = 'שלב 4 - תוכניות';
                    break;

                case 5:
                    title = 'שלב 5 - סיום';
                    break;

                default:
                    title = 'הקמת דירה' ;

            }

           return title;

        }

        function uploadFile(file, category_id) {

            $scope.newPlans[category_id] = file;
            console.log($scope.newPlans);
        }

        function onSave() {

            // first thing - send to the server all new products and add their ids to productsToSave array

            var promises = [];

            if ($scope.newProducts.length > 0){

                angular.forEach($scope.newProducts, function(newProduct){

                    var deferred = $q.defer();
                    promises.push(deferred.promise);

                    if (!newProduct.id) {

                        if (newProduct.file) {

                            FileService.convertFileUriToFile(newProduct.file).then(function (file) {

                                var formData = new FormData();

                                formData.append("file", file);
                                formData.append("title", newProduct.title);
                                formData.append("provider", newProduct.provider);
                                formData.append("refund", String(newProduct.refund));
                                formData.append("description", newProduct.description);
                                formData.append("product_category_id", String(newProduct.product_category_id));

                                Restangular.all('products').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                                    .then(function (data) {

                                        $scope.productsToSave.push(data.id);
                                        deferred.resolve();

                                    })

                            });

                        } else {

                            newProduct.save().then(function(data){

                                $scope.productsToSave.push(data.id);
                                deferred.resolve();

                            })

                        }

                    } else {

                        $scope.productsToSave.push(newProduct.id);
                        deferred.resolve();
                    }


                });

            }

            // now - saving plans

            $q.all(promises).then(function(){

                var promises2 = [];

                if (!angular.equals($scope.newPlans, {})){

                    _.forOwn($scope.newPlans, function(value, key){

                        var deferred2 = $q.defer();
                        promises2.push(deferred2.promise);

                        var formData = new FormData();
                        formData.append(Utils.randomString(), value);

                        Restangular.one('plan_categories', key).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                            .then(function(data) {

                                $scope.savedPlans.push(data[0]);
                                deferred2.resolve();

                            });

                        // FileService.convertFileUriToFile(value).then(function(file){
                        //
                        //     var formData = new FormData();
                        //     formData.append(Utils.randomString(), file);
                        //
                        //     Restangular.one('plan_categories', key).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)
                        //
                        //         .then(function(data) {
                        //
                        //             $scope.savedPlans.push(data[0]);
                        //             deferred2.resolve();
                        //
                        //         });
                        //
                        // })

                    });

                }

                $q.all(promises2).then(function(){

                        // making list of all apartments that must be added

                        _.forEach($scope.selected.apartments, function (apartment) {saveApartment(apartment);});

                });

                }

            );

        }

        // send all apartments to the server

        function saveApartment(key) {

            if (angular.equals({}, $scope.apartmentData.plain())) {

                $scope.apartmentData = _.merge($scope.apartmentData, $scope.selected.apartment.plain());
                $scope.apartmentData.id = null;

            }

            $scope.apartmentData.internal_id = key;

            $scope.apartmentData.save().then(saveApartmentsProducts);

        }

        // link products with apartments

        function saveApartmentsProducts(apartmentData) {

            $scope.apartmentData = apartmentData;
            $scope.savedApartments.push($scope.apartmentData);

            if ($scope.productsToSave.length > 0){

                $q.all(
                    _.map($scope.productsToSave, function (productToSave) {

                        Restangular.one('apartments', $scope.apartmentData.id).one('products', productToSave).all('relationship').post();

                    })).then(saveApartmentsPlans);

            } else
                saveApartmentsPlans();

        }

        function saveApartmentsPlans () {

            if ($scope.savedPlans.length > 0){

                _.each($scope.savedApartments, function (apartment){

                    _.each($scope.savedPlans, function(savedPlan){

                        Restangular.one('apartments', apartment.id).one('plans', savedPlan.id).all('relationship').post();

                    })

                });

            }

            WizardHandler.wizard().next();

        }

        function makeFinish() {

            WizardHandler.wizard().reset();

            Restangular.one('users', $localStorage.id).one('buildings', $stateParams.building_id).all('apartments').getList()
                .then(function(data){

                    $scope.apartments = data;

                    $scope.choice = 0;
                    $scope.selection = 1;
                    $scope.emptyApartments = findEmptyApartments();
                    $scope.selected = {
                        apartment: null,   // 2 step = apartment data
                        apartments: [], // 1 step = apartments numbers
                        products: []   // 3 step = products numbers
                    };
                    $scope.apartmentData = Restangular.restangularizeElement(Restangular.one('buildings', building.id), {}, 'apartments');   // 2 step - info about apartment
                    $scope.product_category = null;
                    $scope.newProducts = Restangular.restangularizeCollection('', [], 'products');
                    $scope.newPlans = {};
                    $scope.productsToSave = [];
                    $scope.savedApartments = [];
                    $scope.savedPlans = [];

                })

        }

        function findEmptyApartments() {

            var emptyApartments = [];

            _.forEach(_.range(1, $scope.building.apartments_quantity + 1), function (value) {
                if (_.findIndex($scope.apartments, ['internal_id', value]) === -1)
                    emptyApartments.push(value)
            });

            return Restangular.restangularizeCollection(Restangular.one('buildings', $stateParams.building_id), emptyApartments, 'apartments');
        }

        function selectProduct (x) {

            $scope.newProducts.push(Restangular.restangularizeElement('', x, 'products'));

        }

        function deselectProduct (x) {

            for (var i = 0; i < $scope.newProducts.length; i++){

                if ($scope.newProducts[i].id == x)
                    $scope.newProducts.splice(i, 1);

            }

        }

        function isProductSelected (x) {

            for (var key = 0; key < $scope.newProducts.length; key++){

                if ($scope.newProducts[key].id == x)
                    return true;

            }

        }


        function selectApartment (x) {

            $scope.selected.apartments.push(x);

        }

        function deselectApartment (x) {

            for (var i = 0; i < $scope.selected.apartments.length; i++){

                if ($scope.selected.apartments[i] == x)
                    $scope.selected.apartments.splice(i, 1);

            }

        }

        function isApartmentSelected (x) {

            for (var key = 0; key < $scope.selected.apartments.length; key++){

                if ($scope.selected.apartments[key] == x)
                    return true;

            }

        }

        function showProductsModal(product_category_id) {

            $ionicModal.fromTemplateUrl('main/templates/modal_products.html', {scope: $scope, animation: 'slide-in-up'})
                .then(function (modal) {

                    for (var j = 0; j < $scope.product_categories.plain().length; j++) {

                        if ($scope.product_categories[j].id == product_category_id) {

                            $scope.product_category = $scope.product_categories[j];

                            var newProduct = Restangular.restangularizeElement('', {}, 'products');
                            newProduct.product_category_id = $scope.product_category.id;

                            $scope.newProducts.push(newProduct);

                        }

                    }

                    $scope.modal = modal;
                    $scope.modal.show();

                });

            $scope.addProduct = function () {

                var newP = Restangular.restangularizeElement('', {}, 'products');
                newP.product_category_id = $scope.product_category.id;

                $scope.newProducts.push(newP);

            }
        }

        function closeProductsModal (){

            for (var l = 0; l < $scope.newProducts.length; l++){

                if ($scope.newProducts[l].product_category_id == $scope.product_category.id){

                    if (typeof($scope.newProducts[l].title) == 'undefined'
                        && typeof($scope.newProducts[l].provider) == 'undefined'
                        && typeof($scope.newProducts[l].description) == 'undefined'
                        && typeof($scope.newProducts[l].refund) == 'undefined'){

                        $scope.newProducts.splice(l, 1);

                    }

                }

            }

            $scope.modal.hide();

        }

    });

'use strict';
angular.module('main')
.controller('ApartmentEditCtrl', function ($ionicScrollDelegate, Utils, $cordovaCamera, $ionicPopover, FileService, $q, $ionicModal, Restangular, $ionicPopup, $scope, apartment, WizardHandler, product_categories, plan_categories, building) {

    $scope.apartment = apartment;
    $scope.product_categories = product_categories;
    $scope.plan_categories = plan_categories;
    $scope.building = building;
    $scope.selection = 1;
    $scope.newProducts = [];
    $scope.product_category = null;
    $scope.plan_category = null;

    $scope.changeSelection = changeSelection;
    $scope.getStep = function getStep (){return WizardHandler.wizard().currentStepNumber();};
    $scope.selectProduct = selectProduct;
    $scope.deselectProduct = deselectProduct;
    $scope.isProductSelected = isProductSelected;
    $scope.saveStep1 = saveStep1;
    $scope.saveStep2 = saveStep2;
    $scope.showDeleteProductPopup = showDeleteProductPopup;
    $scope.showProductsModal = showProductsModal;
    $scope.closeProductsModal = closeProductsModal;
    $scope.planExists = planExists;
    $scope.openPlanPopover = openPlanPopover;
    $scope.deletePlan = deletePlan;
    $scope.getTitle = getTitle;
    $scope.uploadFile = uploadFile;

    $ionicPopover.fromTemplateUrl('main/templates/popover_plan.html', {scope: $scope}).then(function(popover) {$scope.popover = popover;});

    function changeSelection(x){

        $scope.selection = x;
        $ionicScrollDelegate.scrollTop();

    }

    function saveStep1() {

        $scope.apartment.save().then(function(){WizardHandler.wizard().next()});
        // WizardHandler.wizard().next();

    }

    function saveStep2() {

        var promises = [];

        angular.forEach($scope.newProducts, function (newProduct) {

            var deferred = $q.defer();
            promises.push(deferred.promise);

            if (newProduct.file) {

                FileService.convertFileUriToFile(newProduct.file).then(function (file) {

                    var formData = new FormData();

                    formData.append("file", file);
                    formData.append("title", newProduct.title);
                    formData.append("provider", newProduct.provider);
                    formData.append("refund", String(newProduct.refund));
                    formData.append("description", newProduct.description);
                    formData.append("product_category_id", String(newProduct.product_category_id));

                    Restangular.all('products').withHttpConfig({transformRequest: angular.identity}).upload(formData)

                        .then(function (data) {

                            Restangular.one('apartments', $scope.apartment.id).one('products', data.id).all('relationship').post()

                                .then(function(){deferred.resolve();});

                        }, function (error) {

                            console.log(error);

                        });

                });
            }

        });

        $q.all(promises).then(function(){

            WizardHandler.wizard().next();

        });
    }

    function getTitle() {

        var title = '';

        switch(WizardHandler.wizard().currentStepNumber()){

            case 1:
                title = 'שלב 2 - עריכת דירה';
                break;

            case 2:
                title = 'שלב 3 - מפרט';
                break;

            case 3:
                title = 'שלב 4 - תוכניות';
                break;

            case 4:
                title = 'שלב 5 - סיום';
                break;

            default:
                title = 'הקמת דירה' ;

        }

        return title;

    }


    function showDeleteProductPopup(product) {

            $ionicPopup.confirm({
                title: 'Product deletion',
                template: 'Are you sure that you want to delete this product from all apartments?'
            }).then(function(res) {

                if(res) {

                    Restangular.one('products', product.id).all('breakAll').post().then(function(){

                        $ionicPopup.alert({title: "Successfully deleted!"});

                        Restangular.one('apartments', $scope.apartment.id).get().then(function(data){$scope.apartment = data;})

                    });

                }
            });


    }

    function selectProduct (product) {

        Restangular.one('apartments', $scope.apartment.id).one('products', product.id).all('relationship').post()

            .then(function(){

                Restangular.one('apartments', $scope.apartment.id).get()

                    .then(function(data){$scope.apartment = data;})

            });

    }

    function deselectProduct (productid) {

        Restangular.one('apartments', $scope.apartment.id).one('products', productid).all('break').post().then(function(){

            Restangular.one('apartments', $scope.apartment.id).get()

                .then(function(data){$scope.apartment = data;})

        });

    }

    function isProductSelected (x) {

        for (var product = 0; product < $scope.apartment.products.length; product++){

            if ($scope.apartment.products[product].id == x)
                return true;

        }

    }

    function planExists (x) {

        for (var plan = 0; plan < $scope.apartment.plans.length; plan++){

            if ($scope.apartment.plans[plan].plan_category_id == x)
                return true;

        }

    }

    function showProductsModal(product_category_id) {

        $ionicModal.fromTemplateUrl('main/templates/modal_products.html', {scope: $scope, animation: 'slide-in-up'})
            .then(function (modal) {

                for (var j = 0; j < $scope.product_categories.plain().length; j++) {

                    if ($scope.product_categories[j].id == product_category_id) {

                        $scope.product_category = $scope.product_categories[j];

                        var newProduct = Restangular.restangularizeElement('', {}, 'products');
                        newProduct.product_category_id = $scope.product_category.id;

                        $scope.newProducts.push(newProduct);

                    }

                }

                $scope.modal = modal;
                $scope.modal.show();

            });

        $scope.addProduct = function () {

            var newP = Restangular.restangularizeElement('', {}, 'products');
            newP.product_category_id = $scope.product_category.id;

            $scope.newProducts.push(newP);

        }
    }

    function closeProductsModal (){

        for (var l = 0; l < $scope.newProducts.length; l++){

            if ($scope.newProducts[l].product_category_id == $scope.product_category.id){

                if (typeof($scope.newProducts[l].title) == 'undefined'
                    && typeof($scope.newProducts[l].provider) == 'undefined'
                    && typeof($scope.newProducts[l].description) == 'undefined'
                    && typeof($scope.newProducts[l].refund) == 'undefined'){

                    $scope.newProducts.splice(l, 1);

                }

            }

        }

        $scope.modal.hide();

    }


    function openPlanPopover ($event, category) {

        $scope.plan_category = category;

        _.forEach($scope.apartment.plans, function (plan) {

            if (plan.plan_category_id == category.id)
                $scope.plan_category.plan = plan;

        });

        $scope.popover.show($event);

    }

    // function openPlanPopup(mode, category_id) {
    //
    //     if ($scope.popover)
    //         $scope.popover.hide();
    //
    //     $ionicPopup.show({
    //         title: 'בחר/י מקור התמונה',
    //         scope: $scope,
    //         cssClass: 'custom-popup',
    //         buttons: [
    //             {text: 'מצלמה', type: 'button-positive', onTap: function () {addImage(1, mode, category_id);}},
    //             {text: 'גלרייה', type: 'button-calm', onTap: function () {addImage(0, mode, category_id);}},
    //             {text: 'ביטול', type: 'button-assertive', onTap: function () {}}
    //         ]
    //     });
    //
    // }
    //
    // function addImage(index, mode, category_id) {
    //
    //     var options = {
    //         quality: 75,
    //         destinationType: Camera.DestinationType.FILE_URI,
    //         sourceType: index == 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
    //         allowEdit: false,
    //         encodingType: Camera.EncodingType.JPEG,
    //         targetWidth: 600,
    //         targetHeight: 300,
    //         popoverOptions: CameraPopoverOptions,
    //         saveToPhotoAlbum: false,
    //         correctOrientation: true
    //     };
    //
    //     $cordovaCamera.getPicture(options).then(function (fileUri) {
    //
    //         if (mode == 'edit'){
    //
    //             Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post()
    //
    //                 .then(addNewPlan(fileUri, category_id))
    //
    //         }
    //
    //         else
    //             addNewPlan(fileUri, category_id);
    //
    //     });
    // }

    function uploadFile(mode, category_id, file) {

        if ($scope.popover)
            $scope.popover.hide();

        if (mode === 'edit'){

            Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post()

                .then(addNewPlan(file, category_id))

        }

        else
            addNewPlan(file, category_id);

    }

    function addNewPlan(file, category_id) {

        var formData = new FormData();
        formData.append(Utils.randomString(), file);

        Restangular.one('plan_categories', category_id).all('plans').withHttpConfig({transformRequest: angular.identity}).upload(formData)

            .then(function(data) {

                Restangular.one('apartments', $scope.apartment.id).one('plans', data[0].id).all('relationship').post()

                    .then(function() {

                        Restangular.one('apartments', $scope.apartment.id).get().then(function (data) {

                            $ionicPopup.alert({title: "Successfully uploaded!"});
                            $scope.apartment = data;

                        })

                    })

            });

    }

    function deletePlan() {

        Restangular.one('apartments', $scope.apartment.id).one('plans', $scope.plan_category.plan.id).all('break').post().then(function(){

            Restangular.one('plans', $scope.plan_category.plan.id).remove().then(function(){

                Restangular.one('apartments', $scope.apartment.id).get().then(function(data){

                    $scope.apartment = data;
                    $scope.popover.hide();

                })

            })

        });

    }

});

'use strict';
angular.module('main')
    .constant('Config', {

        // gulp environment: injects environment vars
        ENV: {
            /*inject-env*/
            'SERVER_URL': 'http://bonimli.applicazza.net/api'
            /*endinject*/
        },

        // gulp build-vars: injects build vars
        BUILD: {
            /*inject-build*/
            /*endinject*/
        }

    });

'use strict';
angular.module('bonimli', [
  // load your modules here
  'main', // starting with the main module
]);
